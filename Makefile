all: build

resources/gitlab-ee-134.tgz:
		cd resources/ && wget https://s3-eu-west-1.amazonaws.com/gitlab-ee-bosh-release/releases/gitlab-ee/gitlab-ee-134.tgz

build: resources/gitlab-ee-134.tgz
		tile build

.PHONY: all build
