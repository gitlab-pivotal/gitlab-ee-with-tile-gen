exports.migrate = function(input) {
    // Handle converting previous default encoding of `utf8` to `utf8mb4`
    // - check the value of `.gitlab-ee.database.encoding` for `utf8`
    // - set to `utf8mb4` if matched.
    if ( input.properties['.properties.database_encoding'].value === 'utf8' ) {
        console.log('Migrating `database.encoding` from `utf8` to `utf8mb4`');
        input.properties['.properties.database_encoding'].value = 'utf8mb4' ;
    }

    return input ;
}
