FROM cfplatformeng/tile-generator

RUN apk --no-cache add curl py-pip openssl \
    && curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" | tar -C /usr/local/bin -zx \
    && pip install s3cmd && update-ca-certificates
