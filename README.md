# GitLab EE with Tile Generator (PCF Tile)

[Tile Generator][tile] is a tool for developing, packaging, testing, and deploying
services and add-ons for [Pivotal Cloud Foundry][pcf]. GitLab uses this to package
GitLab EE BOSH Release for [PCF][pcf], and is the final packaging for what is placed
into [PivNet][pivnet] as [p-gitlab-ee][p-gitlab-ee].

Tools used here are [`tile`][tile-docs] and [`pcf`][pcf-docs] commands provided by [tile-generator][tile].

Reference material and documentation can be found at https://docs.pivotal.io/tiledev/

For developing, you can follow the installation procedures for the [tile-generator][tile-generator], or use the [`registry.gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen`](https://gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen/container_registry) docker container, which contains `tile` `pcf`, `cf` and `s3cmd` commands pre-installed.

## Updating

1. Clone this repository
  ```bash
  git clone git@gitlab.com:gitlab-pivotal/gitlab-ee-with-tile-gen.git
  ```

1. Change to a new branch for the release (X.Y.Z is the GitLab version)
  ```bash
  git checkout -b release-X.Y.Z
  ```

1. Pull the container
  ```bash
  docker pull registry.gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen
  ```

1. Run the container from your cloned repo path
  ```bash
  docker run -ti -v $(pwd):/tile-gen:rw -v /etc/passwd:/etc/passwd:ro --env HOME=/tmp --name tile-gen -u $(id -u):$(id -g) registry.gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen /bin/bash -l
  ```

1. Change to the mounted `tile-gen` directory
  ```bash
  cd /tile-gen
  ```

1. Edit `scripts/resources` with the latest version of the BOSH release, setting the value of `GITLAB_RELEASE`
  ```
  GITLAB_RELEASE=162
  ```

1. Run `scripts/resources`. This will ensure the versions of the BOSH releases needed are present in `resources/`, and update `tile.yml` with the set versions.
  ```
  scripts/resources
  ```

1. Run the `tile` command to build the `.pivotal` archive.

  > $ tile build --help
  >
  > Usage: tile build [OPTIONS] [VERSION]

    ```
    tile build <version>
    ```
  **Note:** Specify version only if you need to build a particular version of tile.
  If not supplied, the `[VERSION]` argument will be pulled from
  [`tile-history.yml`](tile-history.yml), incremented the last value, and then
  the version will be updated in the file as well. You can also use it the
  command like `tile build minor` to bump to the next minor version
  (1.2.13 => 1.3.0).

1. Test the resulting file on Pivotal's PIE infrastructure. Check out [Testing tile](#testing-tile) for how to.

### Testing tile

1. Copy `metadata_example` file to `metadata` and fill in the credentials from 1password.

1. Import the tile to PIE
  ```bash
  pcf import product/gitlab-ee-1.2.0.pivotal
  ```

1. Now the tile is in PIE, we have to test if the deployment forms are working as intended and a successfull deploy is possible.

1. Configure `cf-cli` to interact with PIE
  1. Fetch the credentials with `pcf cf-info`
  1. Login to PIE API. Its URL is available as `CF API Portal` under `PCF OpsManager` note in 1password Build Vault.

        ```bash
        cf login --skip-ssl-validation -a <url>
        ```
  1. Make sure `gitlab` organization is available
      ```bash
      cf orgs
      ```

  1. If it is not available create it
      ```bash
      cf create-org gitlab
      ```

  1. Set the organization
      ```bash
      cf target -o gitlab
      ```

  1. Make sure `gitlab-ee` space is available
      ```bash
      cf spaces
      ```

  1. If it is not available, create it
      ```bash
      cf create-space gitlab-ee
      ```

  1. Set the organization space
      ```bash
      cf target -o gitlab -s gitlab-ee
      ```

1. Now we have to delete any existing Redis and SQL services
  1. List available services

        ```bash
        cf services
        ```

  1. List the service keys associated with redis and mysql
      ```bash
      cf service-keys redis
      cf service-keys mysql
      ```

  1. Delete the service keys associated with service
      ```bash
      cf delete-service-key redis db -f
      cf delete-service-key mysql db -f
      ```

  1. Delete the service itself
      ```bash
      cf delete-service redis -f
      cf delete-service mysql -f
      ```

1. Create new redis and mysql service as well as their service keys. Note the JSON outputs that is generated by the commands. They are needed in following steps.
  ```bash
  cf create-service p-mysql 100mb mysql
  cf create-service-key mysql db
  cf service-key mysql db
  cf create-service p-redis shared-vm redis
  cf create-service-key redis db
  cf service-key redis db
  cf services
  ```

1. Login to our PIE portal (link in 1password Build Vault under PCF OpsManager as `website`) with credentials from 1password.

1. Confirm that the GitLab Enterprise Edition version just uploaded is available on the left sidebar. Click the `+` button to deploy it. GitLab EE will be added to the dashboard, waiting to be configured. Click that.

1. There are 9 categories that has to be configured, namely
  1. Assign AZs and Networks
  1. GitLab Settings
  1. NGINX Settings
  1. SMTP Settings
  1. Redis Database
  1. SQL Database
  1. NFS
  1. LDAP
  1. Resource Config
  1. Stemcell
1. The configuration of these fields are as follows:
  1. AZ and Network Assignments
      * Network: Infrastructure
      * Click Save
  1. GitLab Settings
      * Route name: gitlab
      * Email From: tile@gitlab.com
      * Initial root password: gitlabTile
      * Click Save
  1. NGINX Settings - No need to change
  1. SMTP Settings
      * Mail Server Address: `hostname` from PCF SMTP Settings in 1password
      * Mail Server Port: 25
      * Mail Server Credentials
          * Username: `username` from PCF SMTP Settings in 1password
          * Password: `password` from PCF SMTP Settings in 1password
      * Mail Server Authentication: login
      * Mail server domain: empty
      * Mail Server TLS : Unchecked
      * Click Save
  1. Configure connection to external Redis instance
      * Redis Hostname: `host` value from redis service key creation
      * Redis Port: `port` value from redis service key creation
      * Redis Password: `password` value from redis service key creation
      * Click Save
  1. Configure SQL Database of GitLab
      * Database Name: `name` value from mysql service key creation. *WARNING!* Replace `gitlabhq_production`!
      * Database encoding: utf8mb4
      * MySQL Hostname: `hostname` value from mysql service key creation
      * DB Port: `port` value from mysql service key creation
      * DB Username: `username` value from mysql service key creation
      * DB Password: `password` value from mysql service key creation
      * Adapter: mysql2
      * Click Save
  1. NFS - No need to change
  1. LDAP
      * Enabled: checked
      * Servers: click Add
         * Label: defaults to `main` (results in label on login page)
         * Host: `host` value from LDAP Test Server in 1password
         * Bind DN: `bind_dn` value from LDAP Test Server in 1password
         * Password: `password` value from LDAP Test Server in 1password
         * Active Directory: `active_directory` value from LDAP Test Server in 1password
         * Base DN: `base` value from LDAP Test Server in 1password
         * Click Save
  1. Resource Config - No need to change
      * While this does not need altered, the defaults are shown below
      * `file_server`: 1 instance, 5GB Persistent Disk, 2 vCPU 4GB RAM 8GB disk
      * `gitlab-ee`: 2 instances, 0 Persistent Disk, 2 vCPU 4GB RAM 32GB disk
  1. Stemcell - No need to change
      * This _should never_ need altered, and is presented as a method to identify / upload a stemcell if the requirements of the Tile are not met. This _should_ not occur unless the PCF environment is reset.
      * If it is missing, consult the 1Password item to find the correct stemcell and download it from here: http://bosh.cloudfoundry.org/stemcells/

1. Once all settings are configured, return to the dashboard.

1. In the top right corner, `INSTALL GitLab EE` will be listed as a pending action

1. Click the `Apply Changes` button to install GitLab EE (Personal suggestion: Get a coffee, this will take quite some time)

1. Head to GitLab instance (link in 1password Build Vault under PCF OpsManager as `GitLab Instance`) that we just installed and see if everything works fine.
  1. Test if LDAP login works. This login should be done under the `main` tab in the login window.
  1. Test if root login works with the initial password we provided during configuration. This login should be done in the `Standard` tag in the login window.

1. If everything works fine, in the PCF Ops Manager dashboard and delete the installed GitLab product
  1. On the `Installation Dashboard`, click on the trash can icon and click `Confirm`
  1. On the `Installation Dashboard`, on the right side panel under `Pending Changes` you should see `DELETE GitLab Enterprise Edition`
  1. Click `Apply Changes`

1. Delete the service keys and services created
  ```bash
    cf delete-service-key redis db -f
    cf delete-service-key mysql db -f
    cf delete-service redis -f
    cf delete-service mysql -f
  ```

## Submit changes

  After testing and confirming that the tile is fully operational:

  1. Check `tile-history.yml`. `version:` key should list the Tile version that was just deployed.
  1. Commit changes & make a Merge Request,

    ```bash
    git add scripts/resources
    git add tile.yml
    git add tile-history.yml
    git commit
    ```

## Uploading tile to PivNet

Once the tile is tested and confirmed to be working fine, it has to be uploaded
to Pivotal Network to create a new release. The steps for this are as
follows:

1. We need to upload two files to PivNet - The `.pivotal` file that was
   generated in the early steps and an Open Source Disclosure File, which is
   generated using the `LICENSE` file we ship with omnibus package.
   Make sure you have both ready.
    1. The `.pivotal` file would be present inside the `product` folder.
    1. In your `gitlab-ee-bosh-release` project, there should be a `.deb` file you could use that was used to build a bosh release. Alternatively, download the corresponding deb file either from S3 bucket or from
       Packagecloud.
    1. Extract the package using `ar` command

        ```bash
        ar x <path to deb file>
        ```
    1. Get the license file from the archive
        ```bash
        tar xf data.tar.* ./opt/gitlab/LICENSE
        ```
    1. Create Open Source Disclosure File by prepending tile version to top of
       LICENSE file
        ```bash
        echo "GitLab Enterprise Edition Premium Tile x.y.z" > OSDF-x.y.z.txt
        cat opt/gitlab/LICENSE >> OSDF-x.y.z.txt
        ```
1. Login to [Pivotal Network](https://network.pivotal.io/products/p-gitlab/)
   using Marin's PivNet credentials available in Build Vault
1. Move on to the [Product Files
   tab](https://network.pivotal.io/products/p-gitlab/product_files)
1. Click on **Create New File** button to reach the file uploading form
1. Click on **Upload a file** and select the `.pivotal` file
1. Give `GitLab Enterprise Premium Tile` as the file name
1. Compute the sha256 sum of the file and provide that value
    ```bash
    # For OS/X
    shasum -a 256 product/gitlab-ee-X.X.X.pivotal

    # For Linux/BSD
    sha256sum <path to .pivotal file>
    ```
1. Give the current version of tile as file version (`version:` value in
   `tile-history.yml` file)
1. No need to provide any signature file
1. Choose `Software` as file type
1. The file date would be auto-filled with today's date
1. In `File Includes` text area, add the text `* GitLab EE x.y.z`
1. Click on Update File (fix any errors if reported)
1. Follow the same procedure to upload LICENSE file. The only changes are as
   follows:
    1. Give `Open Source License File` as the file name
    1. No need to provide sha256 sum for it
    1. Select `Open Source License` as file type

### Creating a new release

After the `.pivotal` file and OSDF has been uploaded, we have to create a new
release that will be provided to the users.
1. In the [Releases](https://network.pivotal.io/products/p-gitlab/admin) tab,
   click the **Create New Release** button
1. In the release creation form, we have to fill necessary information. Though
   PivNet shows a banner saying "Some fields may have been auto-populated with
   values from a previous release.", it actually doesn't fill the fields. Easy
   thing to do is refer an earlier release to fill in the values. Fill in or
   modify the following values
    1. VERSION NAME: The version of the tile
    1. RELEASE TYPE: Major/Minor/Security release, as applicable
    1. RELEASE NOTES URL: `https://docs.pivotal.io/partners/gitlab/release.html`
    1. RELEASE DATE: Date of the release, usually the current date
    1. Dependencies: Modify as needed (modifications arise when we have hard
       requirements on other tiles like `p-mysql` or `p-redis`)
    1. Upgrade path: Modify as needed (depends on the version being uploaded and
       the version series from where a direct upgrade is possible)
    1. Files: Click `Edit Files and File groups` button and add select `GitLab Enterprise Premium Tile X.Y.Z` (.pivotal
       file) and `Open Source License File X.Y.Z` (license file) that we uploaded earlier
    1. Double check that you have `2` files under `Files and File Groups`
    1. Availability: Admin only
    1. EULA: GitLab EULA
    1. LICENSE EXCEPTION: TSU
    1. ECCN: 5D002
1. Click the **Create Release** button. Fix errors if any pop up.

## Updating the GitLab Tile Documentation

Official Docs for GitLab PCF Tile are hosted in a [GitHub repo under pivotal-cf organization](https://github.com/pivotal-cf/docs-gitlab).
As a new release is published, the information provided there has to be updated
to reflect the changes. The steps are
1. Fork the repository and clone it
1. Create a new branch specific to the new version
1. Edit `index.html.md.erb` file and update the Version, Release Date and
   Software Component version specified
1. Edit `release.html.md.erb` file and add a changelog entry corresponding to
   the new version
1. You may refer a previous [Pull
   Request](https://github.com/pivotal-cf/docs-gitlab/pull/12) to confirm you
   made all necessary changes
1. Commit the changes and open a new PR

## Informing Pivotal Partner team about the new release

Once a new release is created and PR for docs are up, we have to inform Pivotal
Partner team about the new release.
1. Contact information is available in Build Vault under `Pivotal Partner
   Contact`
1. The general outline of the message body can be found in this [Google Doc](https://docs.google.com/a/gitlab.com/document/d/13P-4Chlwv-nYkRRY5ofk04-bvgvFYUXggqaGwgfvdOA/edit?usp=sharing) (Internal to GitLab)
1. Pivotal team will confirm everything is ok and make the release public and
   demote older versions, if necessary.

[tile]: https://github.com/cf-platform-eng/tile-generator
[pcf]: https://pivotal.io/platform
[pivnet]: https://network.pivotal.io/
[p-gitlab-ee]: https://network.pivotal.io/products/p-gitlab/
[tile-docs]: https://docs.pivotal.io/tiledev/tile-generator.html
[pcf-docs]: https://docs.pivotal.io/tiledev/pcf-command.html
[bosh-release]: https://gitlab.com/gitlab-pivotal/gitlab-ee-bosh-release


# Maintenance

## Updating the docker container

It will be occasionally required to update the container in order to pick up the latest version(s) of `tile-generator` and `s3cmd` python packages.

1. Find the latest release of `tile-generator`
```bash
curl -I https://github.com/cf-platform-eng/tile-generator/releases/latest 2>/dev/null | g
rep Location | sed s/.*v//
```
The above output appears as `11.0.5`

1. Pull our container
```bash
docker pull registry.gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen:latest
```

1. Find the version of `tile-generator` in our container
```bash
docker run --rm -ti registry.gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen pip show tile-generator | grep Version
```
The above output appears as `Version: 10.0.3`

1. Pull the latest container to the project, so others can use it.
```bash
docker pull cfplatformeng/tile-generator:latest
```

1. Build the container
```
docker build -t registry.gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen .
```

1. Push the updated container
```
docker push registry.gitlab.com/gitlab-pivotal/gitlab-ee-with-tile-gen
```
